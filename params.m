%%Numbers and Initialisations for the main code.

%Initial Conditions
init_cond = [1 0 0 0 10 -5 10];

%Desired endpoint
desired_orientation = [0 0 0 1 0 0 0];

%Time Span
%t_vec = 0:1000:100000;
t_vec = 0:0.05:15;

%Leading Diagonals
I = [0.025 0.025 0.005]';

%Type of Torque 1 = position, 2 = angular velocity, 3 = god, 4 = none 5 =
%imperfect position torque ...... 9 = all disturbance torques 10 = all
%disturbance torques with controller
%Second argument  =1 if disturbance torque is required, 0 if not.

torque_type = [1,0];
%Add external torque?

%Plotting
show_vid = true;
show_omega = false;
show_orientation = false;
show_orbit = false;


%Need 8 vectors for each vertex of the cuboid (x,y,z)
vertex_vec = [0,0,0;
        1,0,0;
        0,1,0;
        0,0,3;
        1,1,0;
        1,0,3;
        0,1,3;
        1,1,3];
 
%Center the cuboid.., do quaternions rotate about origin?   
vertex_vec = vertex_vec - repmat(mean(vertex_vec),size(vertex_vec,1),1);
%Turn vector into quaternion by "adding" real part.
vertex_quat = horzcat([0;0;0;0;0;0;0;0],vertex_vec);

%Arrow vector initialisation
arrow_quat = [0 1 0 0];


%% Athur's Parameters - Usesd to calculate drag and solar torques.
phi = 1366; % solar output (w/m2)
c = 3*10^8;  % speed of light (m/s)
P = phi/c;
rho = 2.43*10^-12; %atmospheric pressure
Cd = 2.5;  % max coefficient of drag for LEO
v = 7.53; % (relative) speed of satellite around the earth % V is the velocity vector of cubesat around earth
Cr = 0.55; % coefficient of reflectance (0.55 for aluminium, 0.1 for solar panels)
Ap = 0.03; % maximum
R = 0.15; % maximum

a = 0.0012637; b = 0.00007898148; % Angular velocity around earth in x and z.