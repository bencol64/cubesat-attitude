function CubeSat()
% Function to create an animation of a 3D rigid body rotating in space
% 
% To use - just type  new_animate() into command line, change parameters in
% params.m file.
%
% Things to play with:
%       -Try changing the initial conditions. first 4 numbers denote initial
%       quaternion orientation, last 3 denote the angular velocities around
%       x,y,z axes.
%
% Things to do:
%       -Add in torque combined with magnetic field.
%       -reference frames
%

%Load the parameters for the Simulation
params;

%ODE finds the rotations...  we need to find the desired rotation from our
%orientation.
desired_var = find_desired_rot(desired_orientation);

%Find the "orientations" and the angular velocities
[t,X] = ode45(@(t,x)eq_motion(t,x,torque_type,I,desired_var, Ap,rho,Cd,R,v,P),t_vec, init_cond);
unnorm_q_list = X(:,1:4);

%Normalise q_list Possibly unnecessary now.
q_list = [];
for i= 1:size(unnorm_q_list,1)
    q_listappend = unnorm_q_list(i,:)./norm(unnorm_q_list(i,:));
    q_list = [q_list;q_listappend];
end

%Plotting and visualisation
if show_omega
    plot_omegas(X,t);
end

if show_orientation
    %Show the orientation of X arrow and Y arrow
    orient_x = quatmultiply(quatmultiply(q_list,[0 1 0 0]),[q_list(:,1), - q_list(:,2:4)]);
    orient_y = quatmultiply(quatmultiply(q_list,[0 0 1 0]),[q_list(:,1), - q_list(:,2:4)]);
    plot_orientation(orient_x,orient_y,t);
end

if show_vid
    animate_cube(q_list,vertex_quat,arrow_quat);
end

if show_orbit
    orb_animate();
end
end

%% Equation of Motion - ODE Format:

function [dx] = eq_motion(t,x,torque_type,I, desired_var, Ap,rho,Cd,R,v,P)
%x = [q1,q2,q3,q4,w1,w2,w3];
%dx = [q'1,q'2,q'3,q'4,w'1,w'2,w'3];

%Choose from the different control torques in the params.m file
switch torque_type(1)
    case 1 
        %Position torque
        torque =0.1* position_torque(I,x(5:7),x(1:4),desired_var);
    case 4
        %torque free
        torque = [0 0 0]';
    case 5 
        %imperfect knowledge
        torque = imperfect_position_torque(I,x(5:7),x(1:4),desired_var,t);
    case 6 
        %aero drag torque!
        velocity = get_velocity(get_position(t));
        velocity_in_frame  = quatmultiply(quatmultiply([x(1),-x(2:4)'],[0,velocity]), x(1:4)');
        torque =dragtorque(velocity_in_frame(2:4));
        %Make drag roughly the right size
        torque = torque*10^(-4);
    case 7
        %gravity gradient torque
        torque = gravity_gradient(get_position(t),I);
  
    case 8
        %TODO: AERO TORQUE -v or v?!?!?!?!
        sun_vector = [0 1 0];
        sun_vector_in_frame = quatmultiply(quatmultiply([x(1),-x(2:4)'],[0,sun_vector]), x(1:4)');
        torque = dragtorque(sun_vector_in_frame(2:4));
        torque = torque;
    case 9 
        %Sun, gravity,drag torques:
        sun_vector = [0.6 0.8 0];
        sun_vector_in_frame = quatmultiply(quatmultiply([x(1),-x(2:4)'],[0,sun_vector]), x(1:4)');
        sun_torque = dragtorque(sun_vector_in_frame(2:4));
        sun_torque = sun_torque*10^(-5);
       
        grav_torque = gravity_gradient(get_position(t),I);
        velocity = get_velocity(get_position(t));
        velocity_in_frame  = quatmultiply(quatmultiply([x(1),-x(2:4)'],[0,velocity]), x(1:4)');
        drag_torque =dragtorque(velocity_in_frame(2:4));
        %Make drag roughly the right size
        drag_torque = drag_torque*10^(-4);

        control_torque = position_torque(I,x(5:7),x(1:4),desired_var);

        %norm(control_torque*10^(-6))
        control_torque =control_torque*10^(-6);
        
        if norm(control_torque) > 10^(-5)
            disp('Error Control torque too big');
            control_torque =( control_torque*10^(-5))/norm(control_torque);
    
        end
        
        %mag_field = get_mag_field(get_position(t));
        %magnetorquer_moment = get_magnetorquer(mag_field, control_torque)
        torque = drag_torque + sun_torque + grav_torque + control_torque;
       
        
   
    otherwise
        disp('Error, Please specify a Control torque');
end

t_ext = 0;
if torque_type(2)
    t_ext = 0.1*ext_torque(t);
end

torque = t_ext + torque;


%Quaternions: Last term also assists in normalisation with thanks to oscar.
dx(1:4) = 0.5*quatmultiply(x(1:4)',[0;x(5:7)]')  + (1 - norm(x(1:4)))*x(1:4)';

%Angular Velocities
dx(5)= (1/I(1))*(torque(1) - (I(3)-I(2))*x(6)*x(7));
dx(6)= (1/I(2))*(torque(2) - (I(1)-I(3))*x(5)*x(7));
dx(7)= (1/I(3))*(torque(3) - (I(2)-I(1))*x(5)*x(6));


if abs(sum(x(1:4).*x(1:4))-1)>5*eps
    format long
    %disp(sum(x(1:4).*x(1:4)));
    %disp('ERROR, Unit quaternion diverging')
end

dx = dx';
end


function [desired_var]=find_desired_rot(desired_orientation)
%find the rotation from [0 1 0 0] to [0 x y z]
%dot product for angle, and cross for normal vector

theta = acos(sum([1 0 0].*desired_orientation(2:4)));
normal_vec = cross([1 0 0],desired_orientation(2:4));
norm_vec = normal_vec./norm(normal_vec);
desired_var = [cos(theta/2), sin(theta/2)*norm_vec,desired_orientation(5:7)];

end

%% Physical numerical Functions

function [mu] = get_magnetorquer(B, t_req)
mu =  cross(t_req,B)/(norm(B)^2);
end

function [B] = get_mag_field(p)
M = -3.12e-5; %Earth's Magnetic Moment
x = p(1); y = p(2); z=p(3);
r = sqrt(x^2+y^2+z^2);
B(1) = 3*M*(x*z)/(r^5);
B(2) = 3*M*(y*z)/(r^5);
B(3) = M*(3*z^2 -r^2)/(r^5);
end

function [p] = get_position(t)
%Earths Radius ; Orbit Height;
r = 6400; R = 400;
%Earths angular velocity = 0.00007898148
%Satellites angular velocity = 0.0012637
a = 0.0011637; b = 0.00007898148;
p(1,:) =(r + R).*cos(a.*t);
p(2,:) = 0;
p(3,:) = (r + R).*sin(a.*t);
p = 1000*p';
end

function [v] = get_velocity(pos)
a = 0.0012637; b = 0.00007898148;
w = [0;a;0];
v = cross(pos,w);
end

%% Torque Functions
%External disturbance torques

function [t_grav] = gravity_gradient(pos,I)
G =  6.674*10^(-11);
M_earth = 5.972*10^(24);

r_o = norm(pos);
t_grav(1) =  ((-3*G*M_earth)/(r_o^5))*((I(2) - I(3))*pos(2)*pos(3));
t_grav(2) =  ((-3*G*M_earth)/(r_o^5))*((I(3) - I(1))*pos(1)*pos(3));
t_grav(3) = 0;
t_grav = t_grav';
end

function[t_aero] = find_aerotorque(Ap,rho,Cd,R,v,V,P)
t_aero = 0.5*rho*Cd*Ap*v^2*cross(V,[0.05 0.05 0.15]');
end

function [t_ext]= ext_torque(t)

%Random, but constant at each time-step
rng(t);
t_ext = [randn(1,3)]';
t_ext = (t_ext+1)*10^(-1);
end

%Internal Control Torque
function [t_pos] = position_torque(I,w,p,desired_var)
%position control torque
p = p';
p_d = desired_var(1:4);

C_1 = 0.1; %Strength of control torque ~ e-6

w_p = C_1*quatmultiply([p_d(1),-p_d(2:4)],p);
w_e = C_1*w;

t(1) = -(w_e(1) + w_p(2));
t(2) = -(w_e(2) + w_p(3));
t(3) = -(w_e(3) + w_p(4));

t_pos = t';
end

function [t_pos] = imperfect_position_torque(I,w,p,desired_var,t)
%Imperfect position control torque - i.e it doesnt know exactly where to
%point, nor how fast it is spinning. But it does know where it wants to
%point. In other words imperfect w = w + a*N~(0,sigma) and p = p +
%b*N~(0,sigma)

%slight problem: p still has to have modulus 1... so add error then
%normalise (also use rng(t) to make sure that ODE45 sees the same torque at
%the same t.

rng(t);
p = p + randn(4,1)/25;
p = p/norm(p);
p = p';
p_d = desired_var(1:4);
w_p = quatmultiply([p_d(1),-p_d(2:4)],p);
w_p = w_p;
%omega doesnt have to be normalised rng(t+2) also same at same t but
%different to p's randomness
rng(t + 2)
C_1 = 1;
w_e = C_1*(w + randn(3,1)/10);

t(1) = -(w_e(1) + w_p(2));
t(2) = -(w_e(2) + w_p(3));
t(3) = -(w_e(3) + w_p(4));

t_pos = t';

end


%% Plotting functions

function plot_omegas(X,t)
figure('Name','Angular Velocities');
plot(t,X(:,5:7),'LineWidth', 4);
ang_v = X(:,5:7);
axis([0 max(t) min(ang_v(:)) max(ang_v(:))]);
title('Angular Velocities with Torque-Free Precession');
legend('\omega_x','\omega_y','\omega_z');
xlabel('Time, s');
ylabel('Angular Velocity, \omega');
end

function plot_orientation(orient_x,orient_y,t)
figure('Name','Orientations ');
plot(t,orient_x(:,2:4),'LineWidth', 3);
title('Orientation Control with imperfect knowledge');
xlabel('Time, s');
ylabel('Orientation, x,y,z')
legend('X_x','X_y','X_z');
grid minor
end

function orb_animate()

figure('Name','Orbit Animation');
t = 0:0.02:10;

for i = 1:range(size(t))
    hold on
    sphere;
    
    pos(1) = sin(5*t(i))*(cos(pi*t(i)));
    pos(2) = sin(5*t(i))*(sin(pi*t(i)));
    pos(3) = cos(5*t(i));
    
    pos = 2*pos;
    a = plot3( pos(1), pos(2), pos(3), '.'); 
    
    grid on 
    axis(3*[-1 1 -1 1 -1 1]);
    pause(0.02) 
end

end

%TODO: EARTH VECTOR PRINT OUT
function animate_cube(q_list, vertex_quat,arrow_quat)
figure('Name','Simulation Animation');
title('Spinning Cuboid');
xlabel('x');ylabel('y');zlabel('z');view(60,30);grid;
qinv_list = horzcat(q_list(:,1),-q_list(:,2:4));

F(size(q_list,1)) = struct('cdata',[],'colormap',[]);
for i = 1:size(q_list,1)
    hold on
    
    cubeco = quatmultiply(quatmultiply(q_list(i,:),vertex_quat ),[q_list(i,1),-q_list(i,2:4)]);
    arrow_x_coordinates = quatmultiply(quatmultiply(q_list(i,:),arrow_quat),qinv_list(i,:));
    arrow_y_coordinates = quatmultiply(quatmultiply(q_list(i,:),[0 0 1 0]),qinv_list(i,:));
    [X,Y,Z] = cube_coordinates(cubeco(:,2:4));
    quiver3(0,0,0,arrow_x_coordinates(2),arrow_x_coordinates(3),arrow_x_coordinates(4), 'LineWidth',3);
    quiver3(0,0,0,arrow_y_coordinates(2),arrow_y_coordinates(3),arrow_y_coordinates(4),'LineWidth',3);
    for j=1:6
        patch(X(:,j),Y(:,j),Z(:,j),'c');
        axis(3*[-1,1,-1,1,-1,1]); 
    end
    
    F(i) = getframe(gcf);
    pause(0.05)
    
    %Clear the old frame ready for the new one.
    cla;
    
end

save F
end

function [X,Y,Z]=cube_coordinates(vertex_vec)
% This gets a bit complicated because the patch function which plots the
% cube needs the coordinates in a weird form. 

%specify the individal vertices
v1 = vertex_vec(1,:);
v2 = vertex_vec(2,:);
v3 = vertex_vec(3,:);
v4 = vertex_vec(4,:);
v5 = vertex_vec(5,:);
v6 = vertex_vec(6,:);
v7 = vertex_vec(7,:);
v8 = vertex_vec(8,:);

%specify the faces
face1 = [v1;v2;v6;v4]; %xz (y = 0)
face2 = [v3;v5;v8;v7]; %xz (y = 1)
face3 = [v1;v2;v5;v3]; %xy (z = 0)
face4 = [v4;v6;v8;v7]; %xy (z = 1)
face5 = [v1;v3;v7;v4]; %yz (x = 0)
face6 = [v2;v5;v8;v6]; %yz (x = 1)

%make vector of X,Y,Z ordinates to go with patch.
X = [face1(:,1),face2(:,1),face3(:,1),face4(:,1),face5(:,1),face6(:,1)];
Y = [face1(:,2),face2(:,2),face3(:,2),face4(:,2),face5(:,2),face6(:,2)];
Z = [face1(:,3),face2(:,3),face3(:,3),face4(:,3),face5(:,3),face6(:,3)];

end

%% DRAG Force

function [torque] = dragtorque(velocity)
params;

normal_vec =  [1 0 0; 0 1 0;
               0 0 1; -1 0 0;
               0 -1 0; 0 0 -1];
           
vec_to_face = [0.05 0 0; 0.05 0 0;
                0 0 0.15; -0.05 0 0;
                0 -0.05 0; 0 0 -0.15];
           
area_list = [0.03, 0.03, 0.01, 0.03, 0.03, 0.01];
          
velocity = velocity/norm(velocity);

%find the dot product of each normal vector and the face vector.
norm_dot_velocity = [];
for i= 1:6
    norm_dot_velocity = [norm_dot_velocity, dot(normal_vec(i,:),velocity)];    
end

%If this dot product is negative or 0 it means that no torque can arise
%from this face. Therefore set all negative ones to 0.
norm_dot_velocity(norm_dot_velocity<0) = 0;
exposed_faces = norm_dot_velocity>0;




%find the parallell components for each face.
norm_vel_components =[];
for i = 1:6
    norm_vel_components =[norm_vel_components, (norm_dot_velocity(i)*normal_vec(i,:))'];
end

%find the perpendicular components for each face.
perp_vel_components =[];
for i = 1:6 
    perp_vel_components =[perp_vel_components, (velocity' - norm_vel_components(:,i))];
end

%find the area visible from direction in velocity.
area_vector = [];
for i = 1:6
    area_vector=[area_vector, abs((area_list(i)*normal_vec(i,:)))'];
end

area_visible = [];
for i = 1:6
    area_visible = [area_visible,exposed_faces(i)*find_exposed_area(area_vector(:,i),velocity)];

end


force = find_force(perp_vel_components,area_visible);
torque = find_torque(force,vec_to_face);
end

function [exposed_area] = find_exposed_area(face_vector, direction)
%finds the area of the plane as seen by the direction vector
%S_direction = |S|cos(a) a = angle between direction and plane normal

direction = direction/norm(direction);
exposed_area = dot(direction,face_vector);
end

function [force] = find_force(perp_vel_components,area_visible)
force = [];
for i=1:6
    force = [force, perp_vel_components(:,i)*area_visible(i)];
end
end

function [torque] = find_torque(force, vec_to_face)

vec_to_face = vec_to_face';
torque = [0 0 0]';

for i =1:6;
    torque = torque + cross(force(:,i),vec_to_face(:,i));
end
end